import Vue from "vue";
import ColorBox from "./components/ColorBox.vue";
import App from "./App.vue";

Vue.component('ColorBox', ColorBox);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
