# Bikalite Colors

Please visit this [URL](https://bikalite.gitlab.io/bikalite-colors/) after pipeline finishes.

## Installation

Node v15.5.1

```bash
# install
npm install

# dev
npm run serve

# build
npm run build
```
